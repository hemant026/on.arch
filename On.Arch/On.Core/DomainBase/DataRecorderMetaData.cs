﻿namespace On.Core.DomainBase
{
    using System;

    public class DataRecorderMetaData
    {
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public long CreatedBy { get; set; }
        public long? LastModifiedBy { get; set; }
    }
}
