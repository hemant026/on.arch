﻿namespace On.Core.DomainBase
{
    public abstract class DomainObjectBase
    {

        public long Id { get; set; }

        protected DomainObjectBase()
            : this(default(long))
        { }

        protected DomainObjectBase(long id)
        {
            Id = id;
        }

        public virtual bool IsNew { get { return Id == default(long); } }
        public DataRecorderMetaData DataRecorderMetaData { get; set; }
    }
}
