﻿namespace On.Core.Extensions.Enums
{
    public enum OrderByDirection
    {
        Ascending,
        Descending
    }
}
