﻿namespace On.Core.Extensions
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Enums;
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> SafeCast<T>(this IEnumerable source)
        {
            return source == null ? null : source.Cast<T>();
        }

        public static int Count(this IEnumerable enumerable)
        {
            return Enumerable.Count(enumerable.Cast<object>());
        }

        public static bool IsEmpty(this IEnumerable enumerableToCheck)
        {
            return enumerableToCheck.Count() == 0;
        }

        public static bool IsNullOrEmpty(this IEnumerable enumerableToCheck)
        {
            return enumerableToCheck == null || enumerableToCheck.Count() == 0;
        }

        public static bool HasSingleItem(this IEnumerable listToCheck)
        {
            return listToCheck.Count() == 1;
        }

        public static IEnumerable<T> Sort<T, TKey>(this IEnumerable<T> listToSort, OrderByDirection sortDirection, Func<T, TKey> selector)
        {
            switch (sortDirection)
            {
                case OrderByDirection.Descending:
                    return listToSort.ToList().OrderByDescending(selector);
                default:
                    return listToSort.ToList().OrderBy(selector);
            }
        }

        public static IEnumerable<T> Page<T>(this IEnumerable<T> enumerableToPage, int pageNumber, int pageSize)
        {
            return enumerableToPage.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> list)
        {
            var collection = new ObservableCollection<T>();
            list.ForEach(collection.Add);
            return collection;
        }

        public static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
        {
            if (list == null) return;

            foreach (T t in list)
                action(t);
        }

        public static void AddRange<T>(this IList<T> list, IList<T> listToBeAdded)
        {
            foreach (var source in listToBeAdded)
                list.Add(source);
        }

        public static void ForEach<T>(this IEnumerator<T> collection, Action<T> action)
        {
            while (collection.MoveNext())
                action(collection.Current);
        }

        public static void TryForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var item in collection)
            {
                try
                {
                    action(item);
                }
                catch { }
            }
        }

        public static void TryForEach<T>(this IEnumerator<T> enumerator, Action<T> action)
        {
            while (enumerator.MoveNext())
            {
                try
                {
                    action(enumerator.Current);
                }
                catch { }
            }
        }
    }
}
