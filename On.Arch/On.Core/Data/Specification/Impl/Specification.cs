﻿namespace On.Core.Data.Specification.Impl
{
    using System;
    using System.Linq.Expressions;
    using Interface;
    using DomainBase;
    //using Utilities;

    public class Specification<TDomain> : ISpecification<TDomain>
        where TDomain : DomainObjectBase
    {
        private readonly Expression<Func<TDomain, bool>> _predicate;
        private readonly Func<TDomain, bool> _predicateCompiled;

        public Specification(Expression<Func<TDomain, bool>> predicate)
        {
            //Guard.Against<ArgumentNullException>(predicate == null, "Expected a non null expression as a predicate for the specification.");
            _predicate = predicate;
            _predicateCompiled = predicate.Compile();
        }

        public Expression<Func<TDomain, bool>> Predicate
        {
            get { return _predicate; }
        }

        public bool IsSatisfiedBy(TDomain domain)
        {
            return _predicateCompiled.Invoke(domain);
        }

        public static Specification<TDomain> operator &(Specification<TDomain> leftHand, Specification<TDomain> rightHand)
        {
            var rightInvoke = Expression.Invoke(rightHand.Predicate, leftHand.Predicate.Parameters);
            var newExpression = Expression.MakeBinary(ExpressionType.AndAlso, leftHand.Predicate.Body, rightInvoke);
            return new Specification<TDomain>(Expression.Lambda<Func<TDomain, bool>>(newExpression, leftHand.Predicate.Parameters));
        }

        public static Specification<TDomain> operator |(Specification<TDomain> leftHand, Specification<TDomain> rightHand)
        {
            var rightInvoke = Expression.Invoke(rightHand.Predicate, leftHand.Predicate.Parameters);
            var newExpression = Expression.MakeBinary(ExpressionType.OrElse, leftHand.Predicate.Body, rightInvoke);
            return new Specification<TDomain>(Expression.Lambda<Func<TDomain, bool>>(newExpression, leftHand.Predicate.Parameters));
        }
    }
}
