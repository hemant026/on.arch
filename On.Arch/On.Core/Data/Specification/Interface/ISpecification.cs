﻿namespace On.Core.Data.Specification.Interface
{
    using System;
    using System.Linq.Expressions;

    public interface ISpecification<TDomain>
    {
        Expression<Func<TDomain, bool>> Predicate { get; }
        bool IsSatisfiedBy(TDomain entity);
    }
}
