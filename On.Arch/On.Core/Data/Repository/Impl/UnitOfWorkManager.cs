﻿namespace On.Core.Data.Repository.Impl
{
    using System;
    using Microsoft.Practices.ServiceLocation;
    //using P.I.Core.Data.Repository.Interface;
    using Interface;
    using State.Interface;

    public static class UnitOfWorkManager
    {
        private static Func<ITransactionManager> _provider;
        private const string LocalTransactionManagerKey = "UnitOfWorkManager.LocalTransactionManager";

        private static readonly Func<ITransactionManager> DefaultTransactionManager = () =>
        {
            var state = ServiceLocator.Current.GetInstance<IState>();
            var transactionManager = state.Local.Get<ITransactionManager>(LocalTransactionManagerKey);
            if (transactionManager == null)
            {
                transactionManager = new TransactionManager();
                state.Local.Put(LocalTransactionManagerKey, transactionManager);
            }
            return transactionManager;
        };

        static UnitOfWorkManager()
        {
            _provider = DefaultTransactionManager;
        }

        public static void SetTransactionManagerProvider(Func<ITransactionManager> provider)
        {
            if (provider == null)
            {
                _provider = DefaultTransactionManager;
                return;
            }
            _provider = provider;
        }

        public static ITransactionManager CurrentTransactionManager
        {
            get
            {
                return _provider();
            }
        }

        public static IUnitOfWork CurrentUnitOfWork
        {
            get
            {
                return _provider().CurrentUnitOfWork;
            }
        }
    }
}
