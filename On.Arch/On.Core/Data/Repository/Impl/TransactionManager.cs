﻿namespace On.Core.Data.Repository.Impl
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Practices.ServiceLocation;
    //using P.I.Core.Data.Repository.Enum;
    //using P.I.Core.Data.Repository.Interface;
    //using P.I.Core.Extensions;
    using On.Core.Data.Repository.Interface;
    using On.Core.Data.Repository.Enum;
    using On.Core.Extensions;
    public class TransactionManager : ITransactionManager
    {
        private bool _disposed;
        private readonly Guid _transactionManagerId = Guid.NewGuid();
        private readonly LinkedList<UnitOfWorkTransaction> _transactions = new LinkedList<UnitOfWorkTransaction>();

        public IUnitOfWork CurrentUnitOfWork
        {
            get
            {
                return CurrentTransaction == null ? null : CurrentTransaction.UnitOfWork;
            }
        }

        public UnitOfWorkTransaction CurrentTransaction
        {
            get
            {
                return _transactions.Count == 0 ? null : _transactions.First.Value;
            }
        }

        public void EnlistScope(IUnitOfWorkScope scope, TransactionMode transactionMode)
        {
            var uowFactory = ServiceLocator.Current.GetInstance<IUnitOfWorkFactory>();

            if (_transactions.Count == 0 || transactionMode == TransactionMode.New || transactionMode == TransactionMode.Supress)
            {
                var txScope = TransactionScopeHelper.CreateScope(UnitOfWorkSettings.DefaultIsolation, transactionMode);
                var unitOfWork = uowFactory.Create();
                var transaction = new UnitOfWorkTransaction(unitOfWork, txScope);
                transaction.TransactionDisposing += OnTransactionDisposing;
                transaction.EnlistScope(scope);
                _transactions.AddFirst(transaction);
                return;
            }
            CurrentTransaction.EnlistScope(scope);
        }

        void OnTransactionDisposing(UnitOfWorkTransaction transaction)
        {
            transaction.TransactionDisposing -= OnTransactionDisposing;
            var node = _transactions.Find(transaction);
            if (node != null)
                _transactions.Remove(node);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (_transactions != null && _transactions.Count > 0)
                {
                    _transactions.ForEach(tx =>
                    {
                        tx.TransactionDisposing -= OnTransactionDisposing;
                        tx.Dispose();
                    });
                    _transactions.Clear();
                }
            }
            _disposed = true;
        }
    }
}
