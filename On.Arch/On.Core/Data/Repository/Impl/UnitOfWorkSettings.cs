﻿namespace On.Core.Data.Repository.Impl
{
    using System.Transactions;

    public static class UnitOfWorkSettings
    {
        public static IsolationLevel DefaultIsolation { get; set; }
        public static bool AutoCompleteScope { get; set; }
    }
}
