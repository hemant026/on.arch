﻿namespace On.Core.Data.Repository.Impl
{
    using Enum;
    using System.Transactions;

    public static class TransactionScopeHelper
    {
        public static TransactionScope CreateScope(IsolationLevel isolationLevel, TransactionMode transactionMode)
        {
            if (transactionMode == TransactionMode.New)
                return new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = isolationLevel });
            return transactionMode == TransactionMode.Supress ? new TransactionScope(TransactionScopeOption.Suppress) : new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = isolationLevel });
        }
    }
}
