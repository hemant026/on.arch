﻿namespace On.Core.Data.Repository.Impl
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using On.Core.Data.Repository.Interface;
    using On.Core.DomainBase;
    //using On.Core.Utilities;
    using On.Core.Data.Specification.Interface;
    using Microsoft.Practices.ServiceLocation;

    public abstract class RepositoryBase<TDomain> : IRepository<TDomain>
        where TDomain : DomainObjectBase
    {
        protected abstract IQueryable<TDomain> RepositoryQuery { get; }

        public IEnumerator<TDomain> GetEnumerator()
        {
            return RepositoryQuery.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return RepositoryQuery.GetEnumerator();
        }

        public Expression Expression
        {
            get { return RepositoryQuery.Expression; }
        }

        public Type ElementType
        {
            get { return RepositoryQuery.ElementType; }
        }

        public IQueryProvider Provider
        {
            get { return RepositoryQuery.Provider; }
        }

        public virtual T UnitOfWork<T>() where T : IUnitOfWork
        {
            var currentScope = UnitOfWorkManager.CurrentUnitOfWork;
            //Guard.Against<InvalidOperationException>(currentScope == null, "No compatible UnitOfWork was found. Please start a compatible UnitOfWorkScope before using the repository.");
            //Guard.TypeOf<T>(currentScope, "The current UnitOfWork instance is not compatible with the repository. Please start a compatible unit of work before using the repository.");
            return ((T)currentScope);
        }

        public abstract void Add(TDomain domain);

        public abstract void Delete(TDomain domain);

        public abstract void Detach(TDomain domain);

        public abstract void Attach(TDomain domain);

        public abstract void Refresh(TDomain domain);

        public abstract TDomain Find(long id);

        public IEnumerable<TDomain> Query(ISpecification<TDomain> specification)
        {
            return RepositoryQuery.Where(specification.Predicate).AsQueryable();
        }

        public IQueryable<TDomain> For<TService>()
        {
            var strategy = ServiceLocator.Current.GetAllInstances<IRepositoryFetchingStrategy<TDomain, TService>>().FirstOrDefault();
            return strategy != null ? strategy.Define(this) : this;
        }
    }
}
