﻿namespace On.Core.Data.Repository.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Transactions;

    //using P.I.Core.Data.Repository.Interface;
    //using P.I.Core.Extensions;
    //using P.I.Core.Utilities;

    using Interface;
    using Extensions;
    //using Utilities;
    public class UnitOfWorkTransaction : IDisposable
    {
        private bool _disposed;
        private TransactionScope _transaction;
        private IUnitOfWork _unitOfWork;
        private IList<IUnitOfWorkScope> _attachedScopes = new List<IUnitOfWorkScope>();

        private readonly Guid _transactionId = Guid.NewGuid();


        public event Action<UnitOfWorkTransaction> TransactionDisposing;

        public UnitOfWorkTransaction(IUnitOfWork unitOfWork, TransactionScope transaction)
        {
            //Guard.Against<ArgumentNullException>(unitOfWork == null, "Expected a non-null UnitOfWork instance.");
            //Guard.Against<ArgumentNullException>(transaction == null, "Expected a non-null TransactionScope instance.");
            _unitOfWork = unitOfWork;
            _transaction = transaction;
        }

        public Guid TransactionId
        {
            get { return _transactionId; }
        }

        public IUnitOfWork UnitOfWork
        {
            get { return _unitOfWork; }
        }

        public void EnlistScope(IUnitOfWorkScope scope)
        {
            //Guard.Against<ArgumentNullException>(scope == null, "Expected a non-null IUnitOfWorkScope instance.");
            _attachedScopes.Add(scope);
            scope.ScopeComitting += OnScopeCommitting;
            scope.ScopeRollingback += OnScopeRollingBack;
        }

        void OnScopeCommitting(IUnitOfWorkScope scope)
        {
            //Guard.Against<ObjectDisposedException>(_disposed, "The transaction attached to the scope has already been disposed.");
            if (!_attachedScopes.Contains(scope))
            {
                Dispose();
                throw new InvalidOperationException("The scope being comitted is not attached to the current transaction.");
            }
            scope.ScopeComitting -= OnScopeCommitting;
            scope.ScopeRollingback -= OnScopeRollingBack;
            scope.Complete();
            _attachedScopes.Remove(scope);
            if (_attachedScopes.Count == 0)
            {
                try
                {
                    _unitOfWork.Flush();
                    _transaction.Complete();
                }
                finally
                {
                    Dispose();
                }
            }
        }

        void OnScopeRollingBack(IUnitOfWorkScope scope)
        {
            //Guard.Against<ObjectDisposedException>(_disposed, "The transaction attached to the scope has already been disposed.");
            scope.ScopeComitting -= OnScopeCommitting;
            scope.ScopeRollingback -= OnScopeRollingBack;
            scope.Complete();
            _attachedScopes.Remove(scope);
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void Dispose(bool disposing)
        {
            if (_disposed)
                return;
            if (disposing)
            {
                if (_unitOfWork != null)
                    _unitOfWork.Dispose();

                if (_transaction != null)
                    _transaction.Dispose();

                if (TransactionDisposing != null)
                    TransactionDisposing(this);

                if (_attachedScopes != null && _attachedScopes.Count > 0)
                {
                    _attachedScopes.ForEach(scope =>
                    {
                        scope.ScopeComitting -= OnScopeCommitting;
                        scope.ScopeRollingback -= OnScopeRollingBack;
                        scope.Complete();
                    });
                    _attachedScopes.Clear();
                }
            }
            TransactionDisposing = null;
            _unitOfWork = null;
            _transaction = null;
            _attachedScopes = null;
            _disposed = true;
        }
    }
}
