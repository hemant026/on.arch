﻿namespace On.Core.Data.Repository.Interface
{
    using System;
    public interface IUnitOfWorkScope : IDisposable
    {
        event Action<IUnitOfWorkScope> ScopeComitting;
        event Action<IUnitOfWorkScope> ScopeRollingback;
        Guid ScopeId { get; }
        void Commit();
        void Complete();
    }
}
