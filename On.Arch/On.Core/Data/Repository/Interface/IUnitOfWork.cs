﻿namespace On.Core.Data.Repository.Interface
{
    using System;
    public interface IUnitOfWork : IDisposable
    {
        void Flush();
    }
}
