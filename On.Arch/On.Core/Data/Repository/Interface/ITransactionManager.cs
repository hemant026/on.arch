﻿namespace On.Core.Data.Repository.Interface
{
    using System;
    using Enum;
    public interface ITransactionManager : IDisposable
    {
        IUnitOfWork CurrentUnitOfWork { get; }
        void EnlistScope(IUnitOfWorkScope scope, TransactionMode mode);
    }
}
