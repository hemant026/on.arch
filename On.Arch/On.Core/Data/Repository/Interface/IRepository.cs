﻿namespace On.Core.Data.Repository.Interface
{
    using System.Collections.Generic;
    using System.Linq;
    using Specification.Interface;
    using DomainBase;
    public interface IRepository<TDomain> : IQueryable<TDomain>
        where TDomain : DomainObjectBase
    {
        T UnitOfWork<T>() where T : IUnitOfWork;

        void Add(TDomain domain);
        void Delete(TDomain domain);
        void Detach(TDomain domain);
        void Attach(TDomain domain);
        void Refresh(TDomain domain);
        TDomain Find(long id);
        IEnumerable<TDomain> Query(ISpecification<TDomain> specification);
        IQueryable<TDomain> For<TService>();
    }
}
