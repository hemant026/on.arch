﻿namespace On.Core.Data.Repository.Interface
{
    using System.Linq;
    using On.Core.DomainBase;

    public interface IRepositoryFetchingStrategy<TDomain, TForService>
        where TDomain : DomainObjectBase
    {
        IQueryable<TDomain> Define(IRepository<TDomain> repository);
    }
}
