﻿namespace On.Core.State.Interface
{
    public interface ISessionState
    {
        T Get<T>();
        T Get<T>(object key);
        void Put<T>(T instance);
        void Put<T>(object key, T instance);
        void Remove<T>();
        void Remove<T>(object key);
        void Clear();
    }
}
