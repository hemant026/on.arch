﻿namespace On.Core.State.Interface
{
    public interface IState
    {
        ISessionState Session { get; }
        //ICacheState Cache { get; }
        //ILocalState Local { get; }
    }
}
