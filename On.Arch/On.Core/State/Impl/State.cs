﻿namespace On.Core.State.Impl
{
    using Interface;
    public class State : IState
    {
        private readonly ISessionState _sessionState;
        //private readonly ICacheState _cacheState;
        //private readonly ILocalState _localState;

        //public State(ISessionState sessionState, ICacheState cacheState, ILocalState localState)
        public State(ISessionState sessionState)
        {
            _sessionState = sessionState;
            //_cacheState = cacheState;
            //_localState = localState;
        }

        public ISessionState Session
        {
            get { return _sessionState; }
        }

        //public ICacheState Cache
        //{
        //    get { return _cacheState; }
        //}

        //public ILocalState Local
        //{
        //    get { return _localState; }
        //}
    }
}
