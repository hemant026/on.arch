﻿namespace On.Data.EntityFrameworkStoredProcedures
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml;

    public static class EntityFrameworkStoredProcedures
    {
        public static StoredProcedureResult CallStoredProcedure<T>(this DbContext context, StoredProcedure<T> procedure, T data)
        {
            var sqlParameters = procedure.Parameters(data).ToList();

            var results = context.ReadFromStoredProcedure(procedure.FullName, sqlParameters, procedure.ReturnTypes);
            procedure.ProcessOutputParameters(sqlParameters, data);

            return results ?? new StoredProcedureResult();
        }

        public static StoredProcedureResult CallStoredProcedure(this DbContext context, StoredProcedure procedure, IEnumerable<SqlParameter> sqlParameters = null)
        {
            var results = context.ReadFromStoredProcedure(procedure.FullName, sqlParameters, procedure.ReturnTypes);
            return results ?? new StoredProcedureResult();
        }

        public static XmlDocument CallXMLStoredProcedure<T>(this DbContext context, StoredProcedure<T> procedure, T data)
        {
            var sqlParameters = procedure.Parameters(data).ToList();
            var results = context.ReadXMLFromStoredProcedure(procedure.FullName, sqlParameters);
            return results ?? new XmlDocument();
        }

        internal static XmlDocument ReadXMLFromStoredProcedure(this DbContext context, String procedureName, IEnumerable<SqlParameter> sqlParameters = null)
        {
            XmlDocument doc = new XmlDocument();
            var connection = (SqlConnection)context.Database.Connection;
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = procedureName;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandTimeout = 60;

                    if (null != sqlParameters)
                        foreach (var sqlParameter in sqlParameters)
                            command.Parameters.Add(sqlParameter);

                    var dataReader = command.ExecuteXmlReader();
                    dataReader.Read();
                    doc.Load(dataReader);

                    dataReader.Close();
                }
            }
            catch (Exception exception)
            {
                var message = string.Format("Error reading from stored procedure {0}: {1}", procedureName, exception.Message);
                throw new Exception(message, exception);
            }
            finally
            {
                connection.Close();
            }

            return doc;
        }

        internal static StoredProcedureResult ReadFromStoredProcedure(this DbContext context, String procedureName, IEnumerable<SqlParameter> sqlParameters = null,
            params Type[] outputTypes)
        {
            var results = new StoredProcedureResult();
            Type[] outType = outputTypes.Reverse().ToArray();
            var currentType = (null == outType) ?
                new Type[0].GetEnumerator() :
                outType.GetEnumerator();

            var connection = (SqlConnection)context.Database.Connection;
            try
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = procedureName;
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.CommandTimeout = 60;

                    if (null != sqlParameters)
                        foreach (var sqlParameter in sqlParameters)
                            command.Parameters.Add(sqlParameter);

                    var dataReader = command.ExecuteReader();

                    if (currentType.MoveNext())
                    {
                        do
                        {
                            var propertyInfos = ((Type)currentType.Current).GetMappedProperties();

                            var currentObject = new List<object>();
                            while (dataReader.Read())
                            {
                                var constructorInfo = ((Type)currentType.Current).GetConstructor(Type.EmptyTypes);
                                if (constructorInfo == null) continue;

                                object item = constructorInfo.Invoke(new object[0]);

                                dataReader.ReadRecord(item, propertyInfos);
                                currentObject.Add(item);
                            }
                            results.Add(currentObject);
                        }
                        while (dataReader.NextResult() && currentType.MoveNext());
                    }
                    dataReader.Close();
                }
            }
            catch (Exception exception)
            {
                var message = string.Format("Error reading from stored procedure {0}: {1}", procedureName, exception.Message);
                throw new Exception(message, exception);
            }
            finally
            {
                connection.Close();
            }

            return results;
        }
    }
}
