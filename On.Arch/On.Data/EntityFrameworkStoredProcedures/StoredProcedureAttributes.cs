﻿namespace On.Data.EntityFrameworkStoredProcedures
{
    using System;
    using System.Data;
    public class StoredProcedureAttributes
    {
        public class Name : Attribute
        {
            public String Value { get; set; }

            public Name(String value)
            {
                Value = value;
            }
        }

        public class Size : Attribute
        {
            public Int32 Value { get; set; }

            public Size(Int32 size)
            {
                Value = size;
            }
        }

        public class Precision : Attribute
        {
            public Byte Value { get; set; }

            public Precision(Byte precision)
            {
                Value = precision;
            }
        }

        public class Scale : Attribute
        {
            public Byte Value { get; set; }

            public Scale(Byte scale)
            {
                Value = scale;
            }
        }

        public class Direction : Attribute
        {
            public ParameterDirection Value { get; set; }

            public Direction(ParameterDirection parameterDirection)
            {
                Value = parameterDirection;
            }
        }

        public class ParameterType : Attribute
        {
            public SqlDbType Value { get; set; }

            public ParameterType(SqlDbType sqlDbType)
            {
                Value = sqlDbType;
            }
        }

        public class TypeName : Attribute
        {
            public String Value { get; set; }

            public TypeName(String typeName)
            {
                Value = typeName;
            }
        }

        public class TableName : Attribute
        {
            public String Value { get; set; }

            public TableName(String tableName)
            {
                Value = tableName;
            }
        }

        public class Schema : Attribute
        {
            public String Value { get; set; }

            public Schema(String schemaName)
            {
                Value = schemaName;
            }
        }
    }
}
