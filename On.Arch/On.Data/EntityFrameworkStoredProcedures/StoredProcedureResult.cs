﻿namespace On.Data.EntityFrameworkStoredProcedures
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class StoredProcedureResult
    {
        readonly List<List<object>> _objects = new List<List<object>>();

        public void Add(List<object> list)
        {
            _objects.Add(list);
        }

        public IEnumerator GetEnumerator()
        {
            return _objects.GetEnumerator();
        }

        public Int32 Count
        {
            get { return _objects.Count; }
        }

        public List<object> this[int index]
        {
            get { return _objects[index]; }
        }

        public List<T> ToList<T>()
        {
            foreach (var objects in _objects.Where(p => p.Count > 0).Select(p => p).Where(list => typeof(T) == list[0].GetType()))
            {
                return objects.Cast<T>().Select(p => p).ToList();
            }

            return new List<T>();
        }

        public T[] ToArray<T>()
        {
            foreach (var objects in _objects.Where(p => p.Count > 0).Select(p => p).Where(list => typeof(T) == list[0].GetType()))
            {
                return objects.Cast<T>().Select(p => p).ToArray();
            }

            return new T[0];
        }
    }
}
