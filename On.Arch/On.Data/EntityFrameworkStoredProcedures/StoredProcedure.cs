﻿namespace On.Data.EntityFrameworkStoredProcedures
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;

    public class StoredProcedure
    {
        public string Schema { get; set; }

        public string ProcedureName { get; set; }

        public StoredProcedure HasOwner(string owner)
        {
            Schema = owner;
            return this;
        }

        public StoredProcedure HasName(string name)
        {
            ProcedureName = name;
            return this;
        }

        public StoredProcedure ReturnsTypes(params Type[] types)
        {
            OutputTypes.AddRange(types);
            return this;
        }

        internal string FullName
        {
            get { return string.Format("{0}.{1}", Schema, ProcedureName); }
        }

        public StoredProcedure()
        {
            Schema = "dbo";
        }

        public StoredProcedure(string procedureName)
        {
            Schema = "dbo";
            ProcedureName = procedureName;
        }

        public StoredProcedure(string name, params Type[] types)
        {
            Schema = "dbo";
            ProcedureName = name;
            OutputTypes.AddRange(types);
        }

        public StoredProcedure(string owner, string name, params Type[] types)
        {
            Schema = owner;
            ProcedureName = name;
            OutputTypes.AddRange(types);
        }


        internal List<Type> OutputTypes = new List<Type>();

        internal Type[] ReturnTypes
        {
            get { return OutputTypes.ToArray(); }
        }
    }

    public class StoredProcedure<T> : StoredProcedure
    {
        public StoredProcedure(params Type[] types)
        {
            Schema = "dbo";

            var schemaAttribute = typeof(T).GetAttribute<StoredProcedureAttributes.Schema>();
            if (null != schemaAttribute)
                Schema = schemaAttribute.Value;

            ProcedureName = typeof(T).Name;

            var procedureNameAttribute = typeof(T).GetAttribute<StoredProcedureAttributes.Name>();
            if (null != procedureNameAttribute)
                ProcedureName = procedureNameAttribute.Value;

            OutputTypes.AddRange(types);
        }

        internal Dictionary<String, String> MappedParameters = new Dictionary<string, string>();

        internal void ProcessOutputParameters(IEnumerable<SqlParameter> parameters, T data)
        {
            var propertyInfos = typeof(T).GetMappedProperties();

            foreach (var sqlParameter in parameters
                .Where(p => p.Direction != ParameterDirection.Input)
                .Select(p => p))
            {
                var propertyName = MappedParameters.Where(p => p.Key == sqlParameter.ParameterName).Select(p => p.Value).First();

                var propertyInfo = propertyInfos.FirstOrDefault(p => p.Name == propertyName);
                if (propertyInfo != null)
                    propertyInfo.SetValue(data, sqlParameter.Value, null);
            }
        }

        internal IEnumerable<SqlParameter> Parameters(T data)
        {
            MappedParameters.Clear();

            var sqlParameters = new List<SqlParameter>();

            foreach (PropertyInfo propertyInfo in typeof(T).GetMappedProperties())
            {
                var sqlParameter = new SqlParameter()
                {
                    ParameterName = propertyInfo.Name
                };

                var parameterName = propertyInfo.GetAttribute<StoredProcedureAttributes.Name>();
                if (null != parameterName)
                    sqlParameter.ParameterName = parameterName.Value;

                var parameterDirection = propertyInfo.GetAttribute<StoredProcedureAttributes.Direction>();
                if (null != parameterDirection)
                    sqlParameter.Direction = parameterDirection.Value;

                var parameterSize = propertyInfo.GetAttribute<StoredProcedureAttributes.Size>();
                if (null != parameterSize)
                    sqlParameter.Size = parameterSize.Value;

                var parameterType = propertyInfo.GetAttribute<StoredProcedureAttributes.ParameterType>();
                if (null != parameterType)
                    sqlParameter.SqlDbType = parameterType.Value;

                var typeName = propertyInfo.GetAttribute<StoredProcedureAttributes.TypeName>();
                if (null != typeName)
                    sqlParameter.TypeName = typeName.Value;

                var precision = propertyInfo.GetAttribute<StoredProcedureAttributes.Precision>();
                if (null != precision)
                    sqlParameter.Precision = precision.Value;

                var scale = propertyInfo.GetAttribute<StoredProcedureAttributes.Scale>();
                if (null != scale)
                    sqlParameter.Scale = scale.Value;

                var value = propertyInfo.GetValue(data, null);
                if (value == null)
                    sqlParameter.Value = DBNull.Value;
                else if (SqlDbType.Structured == sqlParameter.SqlDbType)
                {
                    if (!(value is IEnumerable))
                        throw new InvalidCastException(String.Format("{0} must be an IEnumerable Type", propertyInfo.Name));

                    Type baseType = EntityFrameworkStoredProcedureHelpers.GetUnderlyingType(value.GetType());

                    var schema = propertyInfo.GetAttribute<StoredProcedureAttributes.Schema>();
                    if (null == schema && null != baseType)
                        schema = baseType.GetAttribute<StoredProcedureAttributes.Schema>();

                    var tableName = propertyInfo.GetAttribute<StoredProcedureAttributes.TableName>();
                    if (null == tableName && null != baseType)
                        tableName = baseType.GetAttribute<StoredProcedureAttributes.TableName>();

                    sqlParameter.TypeName = (null != schema) ? schema.Value : "dbo";
                    sqlParameter.TypeName += ".";
                    sqlParameter.TypeName += (null != tableName) ? tableName.Value : propertyInfo.Name;

                    sqlParameter.Value = EntityFrameworkStoredProcedureHelpers.TableValuedParameter((IList)value);
                }
                else
                    sqlParameter.Value = value;

                MappedParameters.Add(sqlParameter.ParameterName, propertyInfo.Name);

                sqlParameters.Add(sqlParameter);
            }

            return sqlParameters;
        }

        public new StoredProcedure<T> HasOwner(String owner)
        {
            base.HasOwner(owner);
            return this;
        }

        public new StoredProcedure<T> HasName(String name)
        {
            base.HasName(name);
            return this;
        }

        public new StoredProcedure<T> ReturnsTypes(params Type[] types)
        {
            base.ReturnsTypes(types);
            return this;
        }
    }
}
