﻿namespace On.Data.EntityFrameworkStoredProcedures
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using Microsoft.SqlServer.Server;

    public static class EntityFrameworkStoredProcedureHelpers
    {
        public static Type GetUnderlyingType(Type type)
        {
            Type baseType = null;
            foreach (Type interfaceType in type.GetInterfaces())
                if (interfaceType.IsGenericType && interfaceType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    baseType = interfaceType.GetGenericArguments()[0];

            return baseType;
        }

        public static PropertyInfo[] GetMappedProperties(this Type type)
        {
            var properties = type.GetProperties();
            var mappedProperties = properties
                .Where(p => p.GetAttribute<NotMappedAttribute>() == null)
                .Select(p => p);

            return mappedProperties.ToArray();
        }

        public static T GetAttribute<T>(this Type type)
            where T : Attribute
        {
            var attributes = type.GetCustomAttributes(typeof(T), false).FirstOrDefault();
            return (T)attributes;
        }

        public static T GetAttribute<T>(this PropertyInfo propertyinfo)
            where T : Attribute
        {
            var attributes = propertyinfo.GetCustomAttributes(typeof(T), false).FirstOrDefault();
            return (T)attributes;
        }

        public static object ReadRecord(this DbDataReader reader, object objectData, PropertyInfo[] propertyInfos)
        {
            // copy mapped properties
            foreach (var propertyInfo in propertyInfos)
            {
                try
                {
                    // default name is property name, override of parameter name by attribute
                    var attributeName = propertyInfo.GetAttribute<StoredProcedureAttributes.Name>();
                    String nameAsPerAttribute = (null == attributeName) ? propertyInfo.Name : attributeName.Value;

                    // get the requested value from the returned dataset and handle null values
                    var data = reader[nameAsPerAttribute];

                    if (data is DBNull)
                        propertyInfo.SetValue(objectData, null, null);
                    else
                        propertyInfo.SetValue(objectData, reader[nameAsPerAttribute], null);
                }
                catch (Exception exception)
                {
                    if (exception is IndexOutOfRangeException)
                    {
                        // if the result set doesn't have this value, intercept the exception
                        // and set the property value to null / 0
                        propertyInfo.SetValue(objectData, null, null);
                    }
                    else
                        throw;
                }
            }

            return objectData;
        }

        public static object ReadRecord(this SqlDataReader reader, object objectData, PropertyInfo[] propertyInfos)
        {
            var nameAsPerAttribute = string.Empty;

            // copy mapped properties
            foreach (var propertyInfo in propertyInfos)
            {
                try
                {
                    // default name is property name, override of parameter name by attribute
                    var attributeName = propertyInfo.GetAttribute<StoredProcedureAttributes.Name>();
                    nameAsPerAttribute = (null == attributeName) ? propertyInfo.Name : attributeName.Value;

                    // get the requested value from the returned dataset and handle null values
                    var data = reader[nameAsPerAttribute];

                    if (data is DBNull)
                        propertyInfo.SetValue(objectData, null, null);
                    else
                        propertyInfo.SetValue(objectData, reader[nameAsPerAttribute], null);
                }
                catch (Exception ex)
                {
                    if (ex is IndexOutOfRangeException)
                    {
                        // if the result set doesn't have this value, intercept the exception
                        // and set the property value to null / 0
                        propertyInfo.SetValue(objectData, null, null);
                    }
                    else
                    {
                        var outerException = new Exception(String.Format("Exception processing return column {0} in {1}",
                            nameAsPerAttribute, objectData.GetType().Name), ex);

                        throw outerException;
                    }
                }
            }

            return objectData;
        }

        internal static IEnumerable<SqlDataRecord> TableValuedParameter(IList table)
        {
            var type = GetUnderlyingType(table.GetType());

            var sqlRecords = new List<SqlDataRecord>();
            var propertyInfos = type.GetMappedProperties();
            var columnInformations = new List<SqlMetaData>();

            var columnMapping = new Dictionary<string, string>();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                // default name is property name, override of parameter name by attribute
                var attributeName = propertyInfo.GetAttribute<StoredProcedureAttributes.Name>();
                var namePerAttribute = (null == attributeName) ? propertyInfo.Name : attributeName.Value;
                columnMapping.Add(namePerAttribute, propertyInfo.Name);

                var columnTypeAttribute = propertyInfo.GetAttribute<StoredProcedureAttributes.ParameterType>();
                var columnType = (null == columnTypeAttribute) ? SqlDbType.Int : columnTypeAttribute.Value;

                SqlMetaData currentColumn;
                switch (columnType)
                {
                    case SqlDbType.Binary:
                    case SqlDbType.Char:
                    case SqlDbType.NChar:
                    case SqlDbType.Image:
                    case SqlDbType.VarChar:
                    case SqlDbType.NVarChar:
                    case SqlDbType.Text:
                    case SqlDbType.NText:
                    case SqlDbType.VarBinary:
                        var columnSizeAttribute = propertyInfo.GetAttribute<StoredProcedureAttributes.Size>();
                        int size = (null == columnSizeAttribute) ? 50 : columnSizeAttribute.Value;
                        currentColumn = new SqlMetaData(namePerAttribute, columnType, size);
                        break;
                    case SqlDbType.Decimal:
                        var precisionAttribute = propertyInfo.GetAttribute<StoredProcedureAttributes.Precision>();
                        var precision = (null == precisionAttribute) ? (byte)10 : precisionAttribute.Value;
                        var scaleAttribute = propertyInfo.GetAttribute<StoredProcedureAttributes.Scale>();
                        var scale = (null == scaleAttribute) ? (byte)2 : scaleAttribute.Value;
                        currentColumn = new SqlMetaData(namePerAttribute, columnType, precision, scale);
                        break;
                    default:
                        currentColumn = new SqlMetaData(namePerAttribute, columnType);
                        break;
                }

                columnInformations.Add(currentColumn);
            }

            foreach (object tableRow in table)
            {
                var sqlDataRecord = new SqlDataRecord(columnInformations.ToArray());

                for (var counter = 0; counter < columnInformations.Count(); counter++)
                {
                    var value = propertyInfos.First(p => p.Name == columnMapping[columnInformations[counter].Name])
                        .GetValue(tableRow, null);

                    sqlDataRecord.SetValue(counter, value);
                }

                sqlRecords.Add(sqlDataRecord);
            }

            return sqlRecords;
        }
    }
}
