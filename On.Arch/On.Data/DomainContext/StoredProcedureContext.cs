﻿namespace On.Data.DomainContext
{
    using System.Collections.Generic;
    using Microsoft.Practices.ServiceLocation;
    using Interface;
    using EntityFramework.Interface;
    using EntityFrameworkStoredProcedures;
    using System.Xml;
    public class StoredProcedureContext<TStoredProcedure, TSoredProcedureResult> : IStoredProcedureContext<TStoredProcedure, TSoredProcedureResult>
    {
        public IReadOnlyList<TSoredProcedureResult> ExecuteStageStoredProcedure(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition)
        {
            using (var context = ServiceLocator.Current.GetInstance<IEntityFrameworkDataContextProvider>().GetExternalDataContext())
            {
                var result = context.CallStoredProcedure(storedProcedureDefinition, storedProcedure);
                return result.ToList<TSoredProcedureResult>();
            }
        }

        public XmlDocument ExecuteNonQueryStoredProcedure(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition)
        {
            using (var context = ServiceLocator.Current.GetInstance<IEntityFrameworkDataContextProvider>().GetDataContext())
            {
                XmlDocument result = context.CallXMLStoredProcedure(storedProcedureDefinition, storedProcedure);
                return result;
            }
        }

        public StoredProcedureResult ExecuteStageStoredProcedureWithMultipleResultSets(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition)
        {
            using (var context = ServiceLocator.Current.GetInstance<IEntityFrameworkDataContextProvider>().GetExternalDataContext())
            {
                var result = context.CallStoredProcedure(storedProcedureDefinition, storedProcedure);
                return result;
            }
        }

        public IReadOnlyList<TSoredProcedureResult> ExecuteStoredProcedure(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition)
        {
            using (var context = ServiceLocator.Current.GetInstance<IEntityFrameworkDataContextProvider>().GetDataContext())
            {
                var result = context.CallStoredProcedure(storedProcedureDefinition, storedProcedure);
                return result.ToList<TSoredProcedureResult>();
            }
        }

        public StoredProcedureResult ExecuteStoredProcedureWithMultipleResultSets(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition)
        {
            using (var context = ServiceLocator.Current.GetInstance<IEntityFrameworkDataContextProvider>().GetDataContext())
            {
                var result = context.CallStoredProcedure(storedProcedureDefinition, storedProcedure);
                return result;
            }
        }
    }
}
