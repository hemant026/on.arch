﻿namespace On.Data.DomainContext.Interface
{
    using System.Collections.Generic;
    using EntityFrameworkStoredProcedures;
    public interface IStoredProcedureContext<TStoredProcedure, out TSoredProcedureResult>
    {
        IReadOnlyList<TSoredProcedureResult> ExecuteStoredProcedure(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition);

        StoredProcedureResult ExecuteStoredProcedureWithMultipleResultSets(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition);

        IReadOnlyList<TSoredProcedureResult> ExecuteStageStoredProcedure(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition);

        StoredProcedureResult ExecuteStageStoredProcedureWithMultipleResultSets(TStoredProcedure storedProcedure,
            StoredProcedure<TStoredProcedure> storedProcedureDefinition);
    }
}
