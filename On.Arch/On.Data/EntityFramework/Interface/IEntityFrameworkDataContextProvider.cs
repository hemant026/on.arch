﻿namespace On.Data.EntityFramework.Interface
{
    using System.Data.Entity;
    public interface IEntityFrameworkDataContextProvider
    {
        DbContext GetDataContext();
        DbContext GetExternalDataContext();
    }
}
