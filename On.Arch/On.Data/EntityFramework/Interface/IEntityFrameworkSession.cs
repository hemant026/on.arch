﻿namespace On.Data.EntityFramework.Interface
{
    using Core.DomainBase;
    using System;
    using System.Data;
    using System.Data.Entity;

    public interface IEntityFrameworkSession : IDisposable
    {
        DbContext Context { get; }
        IDbConnection Connection { get; }
        void Add<TEntity>(TEntity entity) where TEntity : DomainObjectBase;
        void Delete<TEntity>(TEntity entity) where TEntity : DomainObjectBase;
        void Attach<TEntity>(TEntity entity) where TEntity : DomainObjectBase;
        void Detach<TEntity>(TEntity entity) where TEntity : DomainObjectBase;
        void Refresh<TEntity>(TEntity entity) where TEntity : DomainObjectBase;
        IDbSet<TEntity> CreateQuery<TEntity>() where TEntity : DomainObjectBase;
        void SaveChanges();
        TEntity Find<TEntity>(long id) where TEntity : DomainObjectBase;
    }
}
