﻿namespace On.Data.EntityFramework.Interface
{
    using System;
    using System.Data.Entity;
    public interface IEntityFrameworkSessionResolver
    {
        Guid GetSessionKeyFor<T>();
        IEntityFrameworkSession OpenSessionFor<T>();
        DbContext GetContextFor<T>();
        void RegisterContextProvider(Func<DbContext> contextProvider);
        int ContextsRegistered { get; }
    }
}
