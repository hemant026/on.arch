﻿namespace On.Data.EntityFramework.Interface
{
    using Core.Data.Repository.Interface;
    using Core.DomainBase;
    using Impl;

    public interface IEntityFrameworkFetchingRepository<TDomain, TRelated> : IRepository<TDomain>
        where TDomain : DomainObjectBase
    {
        EntityFrameworkRepository<TDomain> RootRepository { get; }
        string FetchingPath { get; }
    }
}
