﻿namespace On.Data.EntityFramework.Impl
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using Microsoft.Practices.ServiceLocation;
    //using P.I.Core.Data.Repository.Impl;
    //using P.I.Core.DomainBase;
    //using P.I.Core.Extensions;
    //using P.I.Data.EntityFramework.Interface;
    using Interface;
    using Core.DomainBase;
    using Core.Extensions;
    using Core.Data.Repository.Impl;

    public class EntityFrameworkRepository<TDomain> : RepositoryBase<TDomain>
        where TDomain : DomainObjectBase
    {
        private readonly IEntityFrameworkSession _privateSession;
        private readonly List<string> _includes = new List<string>();

        public EntityFrameworkRepository()
        {
            if (ServiceLocator.Current == null)
                return;

            var sessions = ServiceLocator.Current.GetAllInstances<IEntityFrameworkSession>().ToList();
            if (!sessions.IsNullOrEmpty())
                _privateSession = sessions.First();
        }

        private IEntityFrameworkSession Session
        {
            get
            {
                if (_privateSession != null)
                    return _privateSession;
                var unitOfWork = UnitOfWork<EntityFrameworkUnitOfWork>();
                return unitOfWork.GetSession<TDomain>();
            }
        }

        protected override IQueryable<TDomain> RepositoryQuery
        {
            get
            {
                var query = Session.CreateQuery<TDomain>().AsQueryable();
                if (_includes.Count > 0)
                    _includes.ForEach(x => query = query.Include(x));
                return query;
            }
        }

        public override void Add(TDomain domain)
        {
            Session.Add(domain);
        }

        public override void Delete(TDomain domain)
        {
            Session.Delete(domain);
        }

        public override void Detach(TDomain domain)
        {
            Session.Detach(domain);
        }

        public override void Attach(TDomain domain)
        {
            Session.Attach(domain);
        }

        public override void Refresh(TDomain domain)
        {
            Session.Refresh(domain);
        }

        public override TDomain Find(long id)
        {
            return Session.Find<TDomain>(id);
        }

        internal void AddInclude(string includePath)
        {
            _includes.Add(includePath);
        }
    }
}
