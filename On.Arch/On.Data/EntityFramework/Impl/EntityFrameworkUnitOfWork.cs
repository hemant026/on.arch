﻿namespace On.Data.EntityFramework.Impl
{
    using System;
    using System.Collections.Generic;
    //using P.I.Core.Data.Repository.Interface;
    //using P.I.Core.Extensions;
    //using P.I.Core.Utilities;
    using Interface;
    //using Core.Utilities;
    using Core.Data.Repository.Interface;
    using Core.Extensions;
    public class EntityFrameworkUnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private readonly IEntityFrameworkSessionResolver _resolver;
        private IDictionary<Guid, IEntityFrameworkSession> _openSessions = new Dictionary<Guid, IEntityFrameworkSession>();

        public EntityFrameworkUnitOfWork(IEntityFrameworkSessionResolver resolver)
        {
            //Guard.Against<ArgumentNullException>(resolver == null, "Expected a non-null EFUnitOfWorkSettings instance.");
            _resolver = resolver;
        }

        public IEntityFrameworkSession GetSession<T>()
        {
            //Guard.Against<ObjectDisposedException>(_disposed, "The current EntityFrameworkUnitOfWork instance has been disposed. Cannot get sessions from a disposed UnitOfWork instance.");

            var sessionKey = _resolver.GetSessionKeyFor<T>();
            if (_openSessions.ContainsKey(sessionKey))
                return _openSessions[sessionKey];

            var session = _resolver.OpenSessionFor<T>();
            _openSessions.Add(sessionKey, session);
            return session;
        }

        public void Flush()
        {
            //Guard.Against<ObjectDisposedException>(_disposed, "The current EFUnitOfWork instance has been disposed. Cannot get sessions from a disposed UnitOfWork instance.");
            _openSessions.ForEach(session => session.Value.SaveChanges());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                if (_openSessions != null && _openSessions.Count > 0)
                {
                    _openSessions.ForEach(session => session.Value.Dispose());
                    _openSessions.Clear();
                }
            }
            _openSessions = null;
            _disposed = true;
        }
    }
}
